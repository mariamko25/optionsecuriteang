
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class PaiementService {
  carte:String;
  propietaire:String;
  date:String;
  csv:String;

  constructor(private logger: NGXLogger,private http: HttpClient, private toastr: ToastrService, private router: Router) {}

  effectuerPaiement(proprietaire,carte,date,csv){
    this.logger.debug(proprietaire,carte,date,csv);


    let list =
    [
      378282246310005,
      371449635398431,
      378734493671000,
      5610591081018250,
      30569309025904,
      38520000023237,
      6011111111111117,
      6011000990139424,
      3530111333300000,
      3566002020360505,
      5555555555554444,
      5105105105105100,
      4111111111111111,
      4012888888881881,
      4222222222222,
      76009244561,
      5019717010103742,
      6331101999990016
    ];

    for (let entry of list) {
      this.logger.debug('checking carte');

      if(carte==entry)
      {
        let cvv=entry%1000;
        if(csv==cvv)
        {
          this.logger.debug('carte & cvc correcte');

            return true;
        }
      }
    }
    this.logger.debug('carte ou cvc incorrecte');

    return false;

  }


}
