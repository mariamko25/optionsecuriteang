import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { CreditCardValidator } from 'angular-cc-library';
import {Item} from '../item/item.interface';

import { Router } from '@angular/router';
import {PaiementService} from './paiement.service';
import {Utilisateur} from '../utilisateur/utilisateur.interface';
import {UtilisateurService} from '../utilisateur/utilisateur.service';
import {Facture,Achat,Carte_Bancaire} from '../commande/commande.interface';
import {CommandeService} from '../commande/commande.service';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.css']
})
export class PaiementComponent implements OnInit {
  form: FormGroup;
  submitted: boolean = false;
  errorMessage:string;
  utilisateur:Utilisateur;
  carte:number;
  private items: Item[] = [];
  private total: number = 0;
  constructor(private logger: NGXLogger,private _fb: FormBuilder,private service:PaiementService,private router: Router,private utilisateurService:UtilisateurService, private commandeService:CommandeService) {}

  ngOnInit() {
    this.logger.debug('preparing to pay');

    this.form = this._fb.group({
      proprietaire:['', [<any>Validators.required, <any>Validators.minLength(5)]],
      creditCard: ['', [<any>Validators.required, <any>Validators.minLength(15), <any>Validators.maxLength(19)]],
      expirationDate: ['', [<any>CreditCardValidator.validateExpDate,<any>Validators.required]],
      cvc: ['', [<any>Validators.required, <any>Validators.minLength(3), <any>Validators.maxLength(4)]],
      saveCard: new FormControl('',)
    });
    this.utilisateurService.getUtilisateurParEmail(localStorage.getItem('email')).subscribe((utilisateur:Utilisateur) =>{
      this.utilisateur=utilisateur;
      this.logger.debug(utilisateur);

       this.commandeService.getCarte(this.utilisateur.idCarte).subscribe((carte:number) =>{
         this.logger.debug(carte);

         if(carte!=null){
            this.carte=carte['numeroCarte'];

         }
       });
    });

  }
  get f() { return this.form.controls; }

  onSubmit(form) {
    this.submitted = true;
    this.logger.debug('Trying to log');

    // stop here if form is invalid
       if (this.form.invalid) {
         this.logger.debug(form);


           return;
       }
       const propietaire=this.form.controls['proprietaire'].value;
       const creditCard=this.form.controls['creditCard'].value;
       const expirationDate=this.form.controls['expirationDate'].value;
       const cvc=this.form.controls['cvc'].value;
       const saveCard=this.form.controls['saveCard'].value;

       //Si la carte doit etre enregistrée

       //Si le paiement est approuvé
       if(this.service.effectuerPaiement(propietaire,creditCard,expirationDate,cvc)){
           if(localStorage.getItem('email')!==null)
           {
              // Je calcule les données du panier
               this.total = 0;
               this.items = [];
               let panier = JSON.parse(localStorage.getItem('panier'));
               if(panier !== null){
               for (var i = 0; i < panier.length; i++) {
                 let item:Item = panier[i];
                 this.items.push({
                   produit: item.produit,
                   quantite: item.quantite
                 });
                 this.total += item.produit.prixUnitaire * item.quantite;
               }
             }
             this.utilisateurService.getUtilisateurParEmail(localStorage.getItem('email')).subscribe((utilisateur: Utilisateur) => {
               this.utilisateur = utilisateur;
               const currentDate= new Date();
               const date= currentDate.getFullYear()+'-'+currentDate.getMonth()+'-'+currentDate.getDate();
               //Je crée une facture
               this.logger.debug('créer facture');

               const facture= new Facture(null,this.utilisateur.idUtilisateur,date,this.total);
               console.log(facture);
               this.commandeService.createFacture(facture);
               //Je crée un achat lié à cette facture
               this.logger.debug('créer achat lié à la facture');

               this.commandeService.getIdLastFacture().subscribe((id:number) =>{
                 var idFacture = 0;
                 for (let val of Object.values(id)) {
                    console.log(val);
                    idFacture=val;
                  }
                  this.logger.debug('idFacture',idFacture);

                 for (var i = 0; i < panier.length; i++) {
                   let item:Item = panier[i];
                   const achat= new Achat(null,item.produit.idProduit,idFacture,item.produit.prixUnitaire,item.quantite);
                   this.logger.debug(achat);

                   this.commandeService.createAchat(achat);

                 }
                 if(saveCard){
                   const carte= new Carte_Bancaire(creditCard,expirationDate);
                   this.logger.debug(carte);
                   this.commandeService.createCarte_Bancaire(carte);
                   var idCarteUser=0;
                   this.commandeService.getidLastCarte_Bancaire().subscribe((idCarte:number) =>{
                     for (let val of Object.values(idCarte)) {
                        idCarteUser=val;
                      }
                      this.logger.debug(idCarteUser);

                      var idUser=this.utilisateur.idUtilisateur;
                      const user= new Utilisateur(this.utilisateur.idUtilisateur,null,null,null,null,null,null,idCarteUser);
                      this.commandeService.insertCarte_BancaireUtilisateur(user);
                   });

                 }
               });
               this.logger.debug(utilisateur);
             }, (err) => {
             console.log(err);
             });
           }

         let panier: any = [];
         localStorage.setItem('panier', JSON.stringify(panier));
         this.router.navigate([`/commande`]);
       }
       else
       {
         this.logger.debug('payment failed');

         this.errorMessage="Erreur le paiement n\'a pas pu avoir lieu !";

       }
  }
}
