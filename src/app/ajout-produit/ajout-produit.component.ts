import { Component, OnInit } from '@angular/core';
import {ProduitService} from '../produit/produit.service';
import {Produit} from "../produit/produit.interface";
@Component({
  selector: 'app-ajout-produit',
  templateUrl: './ajout-produit.component.html',
  styleUrls: ['./ajout-produit.component.css']
})
export class AjoutProduitComponent implements OnInit {

  produits: Produit = {
    idProduit: null,
    intitule: '',
    prixUnitaire: null,
    description: '',
    quantiteDisponible: null,
    categorie: '',
    image:''
  };

  constructor(private produitService: ProduitService) { }

  ngOnInit() {
  }
  createProduit(data: Produit) {
    this.produitService.createProduit(data);
  }
}
