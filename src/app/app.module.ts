import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LayoutModule} from '@angular/cdk/layout';
import {MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule} from '@angular/material';
import {ProduitComponent} from './produit/produit.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ProduitDetailsComponent} from './produit-details/produit-details.component';
import {StorageServiceModule} from "ngx-webstorage-service";

import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from "@angular/common/http";
import {ProduitService} from "./produit/produit.service";
import {UtilisateurService} from "./utilisateur/utilisateur.service";

import {CommandeService} from "./commande/commande.service";

import {PaiementService} from "./paiement/paiement.service";
import {ToastrModule} from "ngx-toastr";
import {ConfirmationPopoverModule} from "angular-confirmation-popover";
import { PanierComponent } from './panier/panier.component';
import { ItemComponent } from './item/item.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { MonCompteComponent } from './mon-compte/mon-compte.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { PaiementComponent } from './paiement/paiement.component';
import { CommandeComponent } from './commande/commande.component';

const appRoutes: Routes = [
  {path: 'produits', component: ProduitComponent},
  {path: 'produitsDetails/:idProduit', component: ProduitDetailsComponent},

  {path: 'aboutUs', component: AboutUsComponent},
  {path: 'panier', component: PanierComponent},
  {path: 'monCompte', component: MonCompteComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'paiement', component: PaiementComponent},
  {path: 'commande', component: CommandeComponent},
  {path: '', component: ProduitComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProduitComponent,
    AboutUsComponent,
    PanierComponent,
    ProduitDetailsComponent,
    ItemComponent,
    UtilisateurComponent,
    MonCompteComponent,
    LoginComponent,
    RegistrationComponent,
    PaiementComponent,
    CommandeComponent,

  ],
  imports: [
    StorageServiceModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    CreditCardDirectivesModule,
    RouterModule.forRoot(appRoutes),
    ToastrModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    LoggerModule.forRoot({serverLoggingUrl: '/api/logs', level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR})
  ],
  providers: [ProduitService,UtilisateurService,PaiementService,CommandeService],
  bootstrap: [AppComponent]
})


export class AppModule { }
