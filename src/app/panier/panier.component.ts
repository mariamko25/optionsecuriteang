import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import {Item} from '../item/item.interface';
import {ProduitService} from '../produit/produit.service';
import {Produit} from '../produit/produit.interface';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {
    private items: Item[] = [];
  	private total: number = 0;



  constructor(private logger: NGXLogger,private activatedRoute: ActivatedRoute,private produitService: ProduitService,private router:Router) {

  }

  ngOnInit() {

    this.logger.debug('showing panier');
    this.activatedRoute.params.subscribe(params => {
        let idProduit:number=params['idProduit'];
  			if (idProduit) {
          this.produitService.getProduitDetails(idProduit).subscribe((produit: Produit) => {
            var item= new Item(produit,1);
    				if (localStorage.getItem('panier') == null) {
    					let panier: any = [];
    					panier.push(item);
    					localStorage.setItem('panier', JSON.stringify(panier));
    				} else {
    					let panier: any = JSON.parse(localStorage.getItem('panier'));
    					let index: number = -1;
    					for (var i = 0; i < panier.length; i++) {
    						let item: Item = panier[i];
                  if (item.produit.idProduit == idProduit) {
      							index = i;
      							break;
      						}


    					}
    					if (index == -1) {
    						panier.push(item);
    						localStorage.setItem('panier', JSON.stringify(panier));
    					} else {
    						let item: Item = panier[index];
    						item.quantite += 1;
    						panier[index] = item;
    						localStorage.setItem("panier", JSON.stringify(panier));
    					}
    				}

    				this.loadpanier();





          }, (err) => {
            this.logger.debug(err);

          console.log(err);
          });

  			} else {
  				this.loadpanier();
  			}
  		});

    }

    loadpanier(): void {
      this.logger.debug('loading panier');

  		this.total = 0;
  		this.items = [];
  		let panier = JSON.parse(localStorage.getItem('panier'));
      if(panier !== null){
  		for (var i = 0; i < panier.length; i++) {
  			let item:Item = panier[i];
  			this.items.push({
  				produit: item.produit,
  				quantite: item.quantite
  			});
  			this.total += item.produit.prixUnitaire * item.quantite;
  		}
    }
  	}


     changeQuantity(idProduit: number,quantity:number):void{
       this.logger.debug('changing quantity',idProduit,quantity);


      let panier: any = JSON.parse(localStorage.getItem('panier'));
              let index: number = -1;
              for (var i = 0; i < panier.length; i++) {
                let item: Item = panier[i];
                  if (item.produit.idProduit == idProduit) {
                    index = i;
                    break;
                  }


              }

                let item: Item = panier[index];
                if(+quantity !==0)
                {
                  item.quantite = +quantity;
                  panier[index] = item;
                  localStorage.setItem("panier", JSON.stringify(panier));
                }
                else
                {
                  this.remove(idProduit);
                }
                  this.loadpanier();


    }
    remove(idProduit: number) {
     let panier: any = JSON.parse(localStorage.getItem('panier'));
     let index: number = -1;
     for (var i = 0; i < panier.length; i++) {
       let item: Item = panier[i];
       if (item.produit.idProduit == idProduit) {
         panier.splice(i, 1);
         break;
       }
     }
     localStorage.setItem("panier", JSON.stringify(panier));
     this.loadpanier();
   }

   viderPanier()
   {
     localStorage.removeItem('panier');
     this.loadpanier();
   }

   validerPanier()
   {
     if(this.total>0){
       this.logger.debug('Trying to log');

       const isLoggedIn = localStorage.getItem('pseudonyme');

        if (isLoggedIn==null) {
          // Si pas d'utilisateur connecté : redirection vers la page de login
          this.logger.debug('Vous n\'êtes pas connectés');

          this.router.navigate(['/login']);
          localStorage.setItem('pageVoulue','paiement');
        }
        else {

          this.router.navigate(['/paiement']);
        }
     }

   }
}
