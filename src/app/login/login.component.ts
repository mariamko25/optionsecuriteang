import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Utilisateur} from '../utilisateur/utilisateur.interface';
import {UtilisateurService} from '../utilisateur/utilisateur.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  model: any = {};
  errorMessage:string;
  utilisateur:Utilisateur;
  constructor(private logger: NGXLogger,private router: Router,private utilisateurService:UtilisateurService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.logger.debug('Trying to log in');

    this.registerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            motDePasse: ['', [Validators.required, Validators.minLength(8)]]
        });
        localStorage.setItem('pageVoulue','monCompte');
  }
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }
  login()  {
    this.submitted = true;
    this.logger.debug('Trying to log in');

    // stop here if form is invalid
       if (this.registerForm.invalid) {
         this.logger.debug('Login failed');

           return;
       }

    const email=this.registerForm.controls['email'].value;
    const motDePasse=this.registerForm.controls['motDePasse'].value;

    const utilisateur={email:email,motDePasse:motDePasse};
    this.logger.debug(utilisateur);

    this.utilisateurService.getAuthentification(utilisateur).subscribe(
    (res:Utilisateur)   => {
       this.logger.debug('Getting user corresponding to auth');

        this.logger.debug(res);

        this.utilisateur=res;
        if(this.utilisateur!==null)
        {

          localStorage.setItem('pseudonyme', this.utilisateur.pseudonyme);
          localStorage.setItem('email', this.utilisateur.email);

          const page= localStorage.getItem('pageVoulue');
          console.log(page);
          this.router.navigate([`/${page}`]);
        }
        else
        {
          this.logger.debug('Login failed');

          this.errorMessage='Votre identifiant ou mot de passe est éronné !';
        }

      },
      err => {
        console.log('Error occured:' , err);
      }
    );


  }
}
