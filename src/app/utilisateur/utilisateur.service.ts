
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {Utilisateur} from './utilisateur.interface';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class UtilisateurService {
  utilisateur: Utilisateur;
  constructor(private logger: NGXLogger,private http: HttpClient, private toastr: ToastrService, private router: Router) {}
  url = 'http://localhost:3000';

  getUtilisateurs(){
    this.logger.debug('getUtilisateurs');

    return this.http.get<Utilisateur[]>(`${this.url}/utilisateurs`);
  }


  getUtilisateur(idUtilisateur){

    this.logger.debug('getUtilisateur');

    this.http.get<Utilisateur>(`${this.url}/utilisateurs/${idUtilisateur}`).subscribe((utilisateur: Utilisateur) => {
      this.utilisateur=utilisateur;
    }, (err) => {
    console.log(err);
    });
    return this.utilisateur;
  }

  getUtilisateurDetails(idUtilisateur){
    this.logger.debug('getUtilisateursDetails');

    return this.http.get<Utilisateur>(`${this.url}/utilisateurs/${idUtilisateur}`);
  }

  getUtilisateurParEmail(email){
    this.logger.debug(email);
    return this.http.get<Utilisateur>(`${this.url}/utilisateurs/email/${email}`);
  }
  createUtilisateur(utilisateur) {
    this.logger.debug('createUtilisateur');
    this.logger.debug(utilisateur);

    this.http.post(`${this.url}/utilisateurs`, utilisateur )
      .subscribe(
        res => {
          this.logger.debug(res);
          this.toastr.success('Votre utilisateur a été créé avec succès.', 'Success');
          this.router.navigateByUrl('/monCompte');
          return true;
        },
        err => {
          this.logger.debug('Error occured:',err);

          this.toastr.error(err.message, 'Error occured');
          return false;
        }
      );
  }

  getAuthentification(utilisateur) {
    this.logger.debug('getAuthentification');

  return  this.http.post(`${this.url}/utilisateurs/auth`, utilisateur);
  }
}
