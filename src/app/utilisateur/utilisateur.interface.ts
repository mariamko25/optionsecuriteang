export class Utilisateur {
  constructor(
     public idUtilisateur: number,
     public nom: string,
     public prenom: string,
     public pseudonyme: string,
     public email: string,
     public motDePasse: string,
     public adresse: string,
     public idCarte: number
  ) { }


}
