import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Utilisateur} from '../utilisateur/utilisateur.interface';
import {UtilisateurService} from '../utilisateur/utilisateur.service';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-mon-compte',
  templateUrl: './mon-compte.component.html',
  styleUrls: ['./mon-compte.component.css']
})
export class MonCompteComponent implements OnInit {

  utilisateur:Utilisateur;
  constructor(private logger: NGXLogger,private router: Router,private utilisateurService:UtilisateurService) { }

  ngOnInit() {
     this.logger.debug('Trying to get to mon compte');

      const isLoggedIn = localStorage.getItem('pseudonyme');
      this.utilisateur= new Utilisateur(null,null,null,null,null,null,null,null);
     if (isLoggedIn==null) {
       // Si pas d'utilisateur connecté : redirection vers la page de login
       this.logger.debug('Vous n\'êtes pas connectés');
       localStorage.setItem('pageVoulue','monCompte');
       this.router.navigate(['/login']);
     }
     else {
      if(localStorage.getItem('email')!==null)
      {
        this.logger.debug('Vous êtes connectés');

        this.utilisateurService.getUtilisateurParEmail(localStorage.getItem('email')).subscribe((utilisateur: Utilisateur) => {
          this.utilisateur = utilisateur;
          this.logger.debug(utilisateur);
        }, (err) => {
        console.log(err);
        });

      }

     }

  }

  logOut()
  {
    this.logger.debug('Logging Out');

    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
