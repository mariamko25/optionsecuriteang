import {Component, OnInit,OnDestroy} from '@angular/core';
import {ProduitService} from '../produit/produit.service';
import {Produit} from '../produit/produit.interface';
import {Router,ActivatedRoute} from "@angular/router";
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-produit-details',
  templateUrl: './produit-details.component.html',
  styleUrls: ['./produit-details.component.css']
})

export class ProduitDetailsComponent implements OnInit {
  produit: Produit;
  idProduit;
  sub;

  constructor(private logger: NGXLogger,private _Activatedroute:ActivatedRoute,private _produitService:ProduitService, private _router:Router) { }

  ngOnInit() {

    this.sub=this._Activatedroute.params.subscribe(params => {
    this.idProduit = params['idProduit'];
    this._produitService.getProduitDetails(this.idProduit).subscribe((produit: Produit) => {
      this.produit = produit;
      this.logger.debug('showing product details');
    }, (err) => {
    console.log(err);
    });

    });

  }
  ngOnDestroy() {
      this.sub.unsubscribe();
  }

  onBack(): void {
      this.logger.debug('trying to go back to produits');

      this._router.navigate(['produits']);
   }

}
