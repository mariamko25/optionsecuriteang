import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Utilisateur} from '../utilisateur/utilisateur.interface';
import {UtilisateurService} from '../utilisateur/utilisateur.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  errorMessage:string;
  constructor(private logger: NGXLogger,private router: Router,private utilisateurService:UtilisateurService,private formBuilder: FormBuilder) { }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
          nom: ['', [Validators.required]],
          prenom: ['', [Validators.required]],
          pseudonyme: ['', [Validators.required]],
          email: ['', [Validators.required, Validators.email]],
          motDePasse: ['', [Validators.required, Validators.minLength(8)]],
          adresse: ['', [Validators.required]]
        });
        this.logger.debug('preparing registering');
  }
  get f() { return this.registerForm.controls; }

  register()
  {
    this.submitted = true;
    this.logger.debug('trying registering');

    // stop here if form is invalid
       if (this.registerForm.invalid) {
         this.logger.debug('form failed');

           return;
       }

    const utilisateur=new Utilisateur(null,
      this.registerForm.controls['nom'].value,
      this.registerForm.controls['prenom'].value,
      this.registerForm.controls['pseudonyme'].value,
      this.registerForm.controls['email'].value,
      this.registerForm.controls['motDePasse'].value,
      this.registerForm.controls['adresse'].value,
      null
    );
      this.utilisateurService.createUtilisateur(utilisateur);
      localStorage.setItem('pseudonyme', JSON.stringify({pseudonyme :this.registerForm.controls['pseudonyme'].value}));
      localStorage.setItem('email',this.registerForm.controls['email'].value);
      const page= localStorage.getItem('pageVoulue');
      this.logger.debug('page voulue',page);


      this.router.navigate([`/${page}`]);




  }
}
