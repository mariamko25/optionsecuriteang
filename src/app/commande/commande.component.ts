import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import {Item} from '../item/item.interface';
import {ProduitService} from '../produit/produit.service';
import {Produit} from '../produit/produit.interface';
import {Utilisateur} from '../utilisateur/utilisateur.interface';
import {UtilisateurService} from '../utilisateur/utilisateur.service';
import {Facture} from './commande.interface';
import {CommandeService} from './commande.service';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {
  private items: Item[] = [];
  private total: number = 0;
  private utilisateur:Utilisateur;
  private factures:Facture[];
  constructor(private logger: NGXLogger,private activatedRoute: ActivatedRoute,private router:Router,private utilisateurService:UtilisateurService, private commandeService:CommandeService) { }

  ngOnInit() {

    this.logger.debug('Getting commands');
    if(localStorage.getItem('email')!==null)
    {

      this.utilisateurService.getUtilisateurParEmail(localStorage.getItem('email')).subscribe((utilisateur: Utilisateur) => {
        this.utilisateur = utilisateur;
        this.commandeService.getFactureParUtilisateur(this.utilisateur.idUtilisateur).subscribe((factures: Facture[]) => {
          this.factures = factures;
        }, (err) => {
        console.log(err);
        });

      }, (err) => {
      console.log(err);
      });

    }


  }

}
