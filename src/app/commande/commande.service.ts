
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {Facture} from './commande.interface';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class CommandeService {
  constructor(private logger: NGXLogger,private http: HttpClient, private toastr: ToastrService, private router: Router) {}
  url = 'http://localhost:3000';



  getFactureParUtilisateur(idUtilisateur){
      this.logger.debug('Getting factures utilisateur');

      return this.http.get<Facture[]>(`${this.url}/factures/${idUtilisateur}`);
  }
  getCarte(idCarte){
      this.logger.debug('Getting carte');

      return this.http.get<number>(`${this.url}/cartes/${idCarte}`);
  }
  createFacture(data) {
    this.logger.debug('Creating Facture');

    this.http.post(`${this.url}/factures`, data)
      .subscribe(
        res => {
          this.toastr.success('Votre facture a été créé avec succès.', 'Success');
          this.logger.debug('Facture créée avec succes');
        },
        err => {
          console.log('Error occured:' , err);
          this.toastr.error(err.message, 'Error occured');
        }
      );
  }

  createAchat(data) {
    this.logger.debug('Creating Facture');

    this.http.post(`${this.url}/achats`, data)
      .subscribe(
        res => {
          this.toastr.success('Votre achat a été créé avec succès.', 'Success');
          this.logger.debug('Votre achat a été créé avec succès');

        },
        err => {
          console.log('Error occured:' , err);
          this.toastr.error(err.message, 'Error occured');
        }
      );
  }

  getIdLastFacture(){
    return this.http.get<number>(`${this.url}/factures/idFacture/desc`);

  }

  getidLastCarte_Bancaire(){
    return this.http.get<number>(`${this.url}/cartes/idCarte/desc`);

  }

  insertCarte_BancaireUtilisateur(data){
    this.http.put(`${this.url}/utilisateurs/insertCarte`,data)
      .subscribe(
        res => {
          this.toastr.success('Votre carte a été insérée avec succès.', 'Success');
          console.log('Votre carte a été insérée avec succès');
        },
        err => {
          console.log('Error occured:' , err);
          this.toastr.error(err.message, 'Error occured');
        }
      );
  }

  createCarte_Bancaire(data){
    this.http.post(`${this.url}/cartes`, data)
      .subscribe(
        res => {
          this.toastr.success('Votre carte a été créé avec succès.', 'Success');
          console.log('Votre carte a été créé avec succès');
        },
        err => {
          console.log('Error occured:' , err);
          this.toastr.error(err.message, 'Error occured');
        }
      );
  }
}
