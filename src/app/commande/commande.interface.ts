export class Facture {
  constructor(
     public idFacture: number,
     public idUtilisateur: number,
     public dateAchat: string,
     public montant: number,
  ) { }


}

export class Achat {
  constructor(
     public idAchat: number,
     public idProduit: number,
     public idFacture: number,
     public prix: number,
     public quantiteAchete:number,
  ) { }


}

export class Carte_Bancaire {
  constructor(
     public numeroCarte: number,
     public dateExpiration: string,
  ) { }


}
