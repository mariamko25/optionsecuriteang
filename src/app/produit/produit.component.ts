import { Component, OnInit } from '@angular/core';
import {ProduitService} from './produit.service';
import {Produit} from './produit.interface';
import {Router} from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css']
})

export class ProduitComponent implements OnInit {
  produits: Produit[];
  constructor(private logger: NGXLogger,private produitService: ProduitService, private router: Router,private http: HttpClient) { }

  ngOnInit() {
    this.logger.debug('getting all products');

  this.produitService.getProduits().subscribe((produits: Produit[]) => {
    this.produits = produits;
  }, (err) => {
  console.log(err);
  });
  }


}
