export class Produit {
  constructor(
     public idProduit: number,
     public intitule: String,
     public prixUnitaire: number,
     public description: String,
     public quantiteDisponible: number,
     public categorie: String,
     public image: String,
  ) { }


}
