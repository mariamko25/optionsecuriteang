
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {Produit} from './produit.interface';
import { NGXLogger } from 'ngx-logger';

@Injectable()
export class ProduitService {
  produit: Produit;
  constructor(private logger: NGXLogger,private http: HttpClient, private toastr: ToastrService, private router: Router) {}
  url = 'http://localhost:3000';

  getProduits(){
    this.logger.debug(' service getProduits');
    return this.http.get<Produit[]>(`${this.url}/produits`);
  }

  getProduit(idProduit){

    this.logger.debug('service getProduit');

    this.http.get<Produit>(`${this.url}/produits/${idProduit}`).subscribe((produit: Produit) => {
      this.produit=produit;
    }, (err) => {
    console.log(err);
    });
    return this.produit;
  }

  getProduitDetails(idProduit){

    this.logger.debug(' service getProduitsDetails');

    return this.http.get<Produit>(`${this.url}/produits/${idProduit}`);
  }
  createProduit(data) {
    this.logger.debug(' service createProduit');

    this.http.post(`${this.url}/produits`, data)
      .subscribe(
        res => {
          console.log(res);
          this.toastr.success('Votre produit a été créé avec succès.', 'Success');
          this.router.navigateByUrl('/produits');
        },
        err => {
          console.log('Error occured:' , err);
          this.toastr.error(err.message, 'Error occured');
        }
      );
  }
}
